import React from 'react';
import './LoteryNumber.css';

const LotteryNumber = props => {
    return (
        <div className="LotteryNumber">
            <h1>{props.number}</h1>
        </div>
    )
}

export  default LotteryNumber;