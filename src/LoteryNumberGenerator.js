import React from "react";

export const generateArray = (size) => {
    const arr = [];
    for (let i = 0; i < size; i++) {
        while (true) {
            const number = Math.round(Math.random() * (32 - 5) + 5);
            if(arr.includes(number)) {
                continue;
            }
            arr.push(number);
            break;
        }
    }

    return arr;
}
