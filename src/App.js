import './App.css';
import {Component} from "react";
import LotteryNumber from './LoteryNumber/LotteryNumber';
import {generateArray} from './LoteryNumberGenerator';

class App extends Component {
   state = {
       lotNumbs : [undefined, undefined, undefined,undefined,undefined]
   }
    render() {
    return (
       <div className="lottery">
           <div id='btnGenerate'>
               <button onClick={this.generateNumbers}>New numbers</button>
           </div>
           <div className="lotteryNumbers">
               <LotteryNumber number = {this.state.lotNumbs[0]}/>
               <LotteryNumber number = {this.state.lotNumbs[1]}/>
               <LotteryNumber number = {this.state.lotNumbs[2]}/>
               <LotteryNumber number = {this.state.lotNumbs[3]}/>
               <LotteryNumber number = {this.state.lotNumbs[4]}/>
           </div>
       </div>
    );
  }

    generateNumbers = () => {
       for(let i=0; i<1000; i++){
            setTimeout(()=>{
               this.setState({lotNumbs: generateArray(5)});
           }, 500);
       }
    };
}

export default App;
